var app = angular.module('tinyurlApp');

app.controller("homeController", ["$scope","$window","$http","$location","$route", function($scope,$window,$http,$location,$route){//http from ngResource
    $scope.login = {userid: "", password: ""};//for sign in
    $scope.logup = {useridNew: "", passwordNew: "",password2New: ""};//for sign in
    $scope.useridNow = '';
    //
    $scope.urlCounts = 0;
    $scope.urls = [];
    
    
    
    
    /////////////////submit function
    $scope.submit = function(){//location is from route
        //console.log($scope.longUrl);
        $http.post("/api/v1/urls",{//post json, url is api/v1/urls, the object is a json file 
            longUrl: $scope.longUrl,
            userid: $scope.useridNow
        })
            .success(function(data){//data is the responsed json file, if success then redirect by using location
                $location.path("/urls/" + data.shortUrl);
            });
    }
    $scope.submit2 = function(shortUrl){
        $location.path("/urls/" + shortUrl);
        /*$http.post("/api/v1/urls",{//post json, url is api/v1/urls, the object is a json file 
            longUrl: longUrl,
            userid: $scope.useridNow
        })
            .success(function(data){//data is the responsed json file, if success then redirect by using location
                $location.path("/urls/" + data.shortUrl);
            });*/
    }
    //////////////////
    
    
    
    
    
    
    $scope.signIn = function() {//sign in function
        
        if($scope.login.userid === undefined || $scope.login.userid === "" || $scope.login.password === undefined || $scope.login.password === "") {//empty
            $scope.alertMe('Please input username and password!');
        }
        
        else {//input has username & password,check if exist
            
            
            $http.get("/api/v1/user/" + $scope.login.userid + "/" + $scope.login.password)
                    .success(function(urls){//data is the responsed json file
                        
                        
                        
                        if(urls === 'invalid') {//user does not exist
                            $scope.alertMe('Sign in failed! Invalid username!');
                            $scope.login = {userid: "", password: ""};//reset input
                            
                        }
                        if(urls === 'wrong') {//password not matched
                            $scope.alertMe('Sign in failed! wrong pass word!');
                            $scope.login = {password: ""};//reset input
                                
                        }
                        else if(urls === '0record') {//no url record for this user now
                            $scope.useridNow = $scope.login.userid;
                            
                        }
                        else{//there are url(s) for this user
                            $scope.useridNow = $scope.login.userid;
                            $scope.urlCounts = urls.length;
                            $scope.urls = urls;
                            
                        }
                        
                    });
        }
    }
    ///////////////////
    
    
    $scope.signUp = function() {//sign up function
        
        if($scope.logup.useridNew === undefined || $scope.logup.useridNew === "" || $scope.logup.passwordNew === undefined || $scope.logup.passwordNew === "" || $scope.logup.password2New === undefined || $scope.logup.password2New === "") {//empty
            $scope.alertMe('Invalid username or password!');
        }
        
        else {//input has username & password
            var useridNew = $scope.logup.useridNew;
            var passwordNew = $scope.logup.passwordNew;
            var password2New = $scope.logup.password2New;
            if(password2New != passwordNew){
                $scope.alertMe('Password not comfirmed!');
            }
            else{//create a new user in DB
                $http.post("/api/v1/user",{//post json, the object is a json file 
                    useridNew: $scope.logup.useridNew,
                    passwordNew: $scope.logup.passwordNew
                })
                    .success(function(userInfo){//data is the responsed json file
                        //userInfo will let the controller know if the user is alreay exist or a new one is created
                        
                        console.log(userInfo);
                        
                        if(userInfo === '1') {//new one created
                            $scope.alertMe('Signing up success!');
                            $scope.logup = {useridNew: "", passwordNew: "",password2New: ""};//reset input
                            $scope.useridNow = useridNew;
                            //then show the id on site 
                            
                        }
                        if(userInfo === '0') {//User already exists
                            $scope.alertMe('User already exists! Signing up failed!');
                            $scope.logup = {useridNew: "", passwordNew: "",password2New: ""};//reset input
                        }
                        
                    });
                    
    
            }
                        
        }
    }
    
    $scope.signOut = function() {//sign in as guest
        $scope.useridNow = "";
        $route.reload();
    }
    
    $scope.alertMe = function(alertMessage) {
        setTimeout(function() {
          $window.alert(alertMessage);
        });
    };
}]);