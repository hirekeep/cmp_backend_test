/*var app = angular.module('tinyurlApp');


app.controller("cmpController", ["$scope", "$http",function($scope,$http){
    $scope.matchesRecords = [];
    //Salesforce
    /////
    $http.get("/api/v1/cmp")
                    .success(function(Candidates){//data is the responsed json file
                    console.log(Candidates.length);
                    $scope.matchesRecords = Candidates;
                    
                    });
    
}]);*/

var app = angular.module('tinyurlApp');

app.controller("cmpController", ["$scope","$window","$http","$location","$route","$cookies", function($scope,$window,$http,$location,$route,$cookies){//http from ngResource
    $scope.matches = [];
    var currentCandidate;//current
    $scope.CandidateEmailInput;//input
    var getCookieCandidate;//cookie
    
    //this part happens when the page is loaded, before you click test
    //if the cookie holds candidate, the input and test button will not show up, the page will be loaded based on the candidate info from cookie
    
    if(typeof($cookies.get('Candidate')) == "undefined" || typeof($cookies.get('Candidate')) == null){//if no candidate in cookie, put the current input into it
                //$cookies.put('Candidate',currentCandidate);
                //getCookieCandidate = $cookies.get('Candidate');
    }
    
    else{//if the cookie has the candidate
            getCookieCandidate = $cookies.get('Candidate');
            currentCandidate = getCookieCandidate;
            $scope.CandidateEmailInput = getCookieCandidate;
            $http.get("/api/v1/cmp/" + currentCandidate)
                        .success(function(resData){//data is the responsed json file


                            console.log("Amount of getting:" + resData.length);
                            $scope.matches = resData;

                        });
    }
    
    console.log(getCookieCandidate);
    
    $scope.test = function(){
        //need to validate first!
        
        //after validation...
        currentCandidate = $scope.CandidateEmailInput;
        
        
        if(typeof(getCookieCandidate) == "undefined" || typeof(getCookieCandidate) == null){//if no candidate in cookie, put the current input into it
                $cookies.put('Candidate',currentCandidate);
                getCookieCandidate = $cookies.get('Candidate');
        }
        
        
        
        
        $http.get("/api/v1/cmp/" + getCookieCandidate)
                    .success(function(resData){//data is the responsed json file
                        
            
                        console.log("Amount of getting:" + resData.length);
                        $scope.matches = resData;
    
                    });
    };
    
    $scope.accept = function(matcheIDParameter){
        $http.post("/api/v1/cmp",{//post json, the object is a json file 
                    matcheID: matcheIDParameter,
                    updatingStage: 'acceptMatche'
                })
                    .success(function(returnCallBack){//data is the responsed json file
                        //userInfo will let the controller know if the user is alreay exist or a new one is created
                        if(returnCallBack == 'fail'){
                            console.log('In controller: accept fail!');
                        }
                        if(returnCallBack == 'success'){
                            console.log('In controller: accept success! And data re-querying...');
                            $http.get("/api/v1/cmp/" + getCookieCandidate)
                                .success(function(resData){//data is the responsed json file


                                    console.log(resData.length);
                                    $scope.matches = resData;
    
                    });
                        }
                        
                        
                    });
    };
    
    $scope.reject = function(matcheIDParameter){
        $http.post("/api/v1/cmp",{//post json, the object is a json file 
                    matcheID: matcheIDParameter,
                    updatingStage: 'rejectMatche'
                })
                    .success(function(returnCallBack){//data is the responsed json file
                        //userInfo will let the controller know if the user is alreay exist or a new one is created
                        if(returnCallBack == 'fail'){
                            console.log('In controller: reject fail!');
                        }
                        if(returnCallBack == 'success'){
                            console.log('In controller: reject success! And data re-querying...');
                            $http.get("/api/v1/cmp/" + getCookieCandidate)
                                .success(function(resData){//data is the responsed json file


                                    console.log(resData.length);
                                    $scope.matches = resData;
    
                    });
                        }
                        
                        
                    });
    };
    
}]);

