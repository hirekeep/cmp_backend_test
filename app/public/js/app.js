var app = angular.module('tinyurlApp',[ 'ngRoute','ngResource','chart.js','ui.bootstrap','ngCookies']);

app.config(function ($routeProvider){
    $routeProvider
        .when("/",{
            templateUrl: "./public/view/home.html",
            controller: "homeController"
        })
        .when("/cmp.html",{
            templateUrl: "./public/view/cmp.html",
            controller: "cmpController"
        });
        
    
    
});