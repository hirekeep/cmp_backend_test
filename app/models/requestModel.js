//the reqInfo from redirect and this one is for storing the reqInfo into DB
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RequestSchema = new Schema({
    shortUrl: String,
    referer: String,
    platform: String,
    browser: String,
    country: String,
    user: String,
    timestamp: Date
});

var requestModel = mongoose.model('RequestModel', RequestSchema);

module.exports = requestModel;