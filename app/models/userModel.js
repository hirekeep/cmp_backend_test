var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    userid: String,
    password: String
});

var userModel = mongoose.model('userModel', UserSchema);

module.exports = userModel;