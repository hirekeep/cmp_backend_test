var geoip = require('geoip-lite');
var RequestModel = require('../models/requestModel');
var logRequest = function(shortUrl,req) {
    var reqInfo = {};
    reqInfo.shortUrl = shortUrl;
    reqInfo.referer = req.headers.referer || 'Unknown';//the clicking website
    reqInfo.platform = req.useragent.platform || 'Unknown';
    reqInfo.browser = req.useragent.browser || 'Unknown';//this useragent is added into the req in the server.js when the express useragent is called to refine the req
    var ip = req.headers['x-forwarded-for'] || 
        req.connection.remoteAddress || 
        req.socket.remoteAddress || 
        req.connection.socket.remoteAddress;//covered all kinds of formats from different browsers
    var geo = geoip.lookup(ip);//from geoip-lite
    if(geo){
        reqInfo.country = geo.country;
    }
    else{
        reqInfo.country = 'Unknown';
    }
    reqInfo.timestamp = new Date();//time
    //all the information is stored in reqInfo
    //Then send it to DB:
    var request = new RequestModel(reqInfo);
    request.save();
    //------------
};

var getUrlInfo = function(shortUrl, info, callback) {
    if(info === 'totalClicks') {//how many times was this short one is clicked, get it from the DB
        RequestModel.count({ shortUrl: shortUrl}, function(err, data) {//from mongoDB
            callback(data);//this count function from MongoDB will give the data(int the mongoDB callback) a value of how many records is in this model(form)
            //then this data is send into the getUrlInfo's callback function to the suprior function
        });
        return;
    }
    
        var groupId = "";
        if(info === 'hour') {
            groupId = {
                year: {$year: "$timestamp"},
                month: {$month: "$timestamp"},
                day: {$hour: "$timestamp"},
                hour: {$hour: "$timestamp"},
                minute: {$minute: "$timestamp"}//match year&& month&&......
            }
        } else if(info === 'day') {
            groupId = {
                year: {$year: "$timestamp"},
                month: {$month: "$timestamp"},
                day: {$hour: "$timestamp"},
                hour: {$hour: "$timestamp"}
            }
        }
        else if(info === 'month') {
            groupId = {
                year: {$year: "$timestamp"},
                month: {$month: "$timestamp"},
                day: {$hour: "$timestamp"}
            }
        }
        else {
            groupId = "$" + info;//it can be country, referer, platform.......all the information you get from the ip
        //they are put into a variable and it is used by the aggregate function in mongoDB to decide the attribute you want to aggregate by(_id)
        }
        
        RequestModel.aggregate([
            {
                $match: {//the first array needs three params(format is from mongoDB)
                    shortUrl:shortUrl//key
                }
            },
            {
                $sort: {
                    timestamp: -1//sorting rule
                }
            },
            {
                $group: {
                    //what you want to aggregate
                    _id: groupId,//different groups
                    count: {
                        $sum: 10
                    }
                }
            }
        ], function (err, data) {
            callback(data);
            //the data will return two information:
            //the _id and count result
        });
    
};
module.exports = {
    logRequest: logRequest,
    getUrlInfo: getUrlInfo
};//this part is to let the outsider, who used this serviceJS file, use the function in js