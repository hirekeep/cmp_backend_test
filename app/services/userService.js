var UserModel = require("../models/userModel.js");
var UrlModel = require("../models/urlModel.js");
var createUser = function(useridNew, passwordNew,callback){
    UserModel.findOne({userid: useridNew}, function(err, userInfo){//find the user from DB, according to the user(in the userModel collection)
                if(userInfo){//result exists, login is the returned object from mongoose(from mongoDB)
                    
                    callback('0');//this is the callback function 
                    
                    
                }
                else{//create a user
                    
                    var userInfo = new UserModel({ userid: useridNew, password: passwordNew});// see the model schema
                    userInfo.save();//this url is stored in DB, WRITE MONGODB!
                    /*var userInfoReturn = userInfo;;
                    
                    userInfoReturn.newOrOld = 'new';
                    console.log(userInfoReturn);
                    console.log(userInfo);*/
                    callback('1');//send back to the callback function
                    
                }
    });
}

var getUrlByUser = function(userid, password,callback){
    
    UserModel.findOne({userid: userid}, function(err, userInfo){//find the user from DB, according to the user(in the userModel collection)
                if(userInfo){//result exists, login is the returned object from mongoose(from mongoDB)
                    if(userInfo.password === password){
                        UrlModel.find({userid: userid}, function(err, urls){
                            if(urls){//it has urls
                                callback(urls);//this is the callback function 
                            }
                            else{//there are no url for this user but user exists
                                callback('0record');
                            }
                        
                        });
                    }
                    else{//wrong password
                        callback('wrong');
                    }

                }
                else{//no such a user
                    
                    callback('invalid');
                    
                }
    });
}



module.exports = {
    createUser: createUser,
    getUrlByUser: getUrlByUser
};//this part is to let the outsider, who used this serviceJS file, use the function in js