//router using express
var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();//sent in json



var statsService = require('../services/statsService');
var userService = require('../services/userService');
var cmpService = require('../services/cmpService');

//totalclicks and the other analystic handler
router.get("/urls/:shortUrl/:info", function (req, res) {
    statsService.getUrlInfo(req.params.shortUrl, req.params.info, function (data) {
        res.json(data);
    });
});
///user
router.post('/user', jsonParser, function (req, res) {//post: create a user
    userService.createUser(req.body.useridNew, req.body.passwordNew,function (userInfo) {//this is the call back function due to the ansyncronized IO of nodejs
                    //userInfo will let the controller know if the user is alreay exist or a new one is created
                    //if(userInfo == '0'){//already exists
                        res.json(userInfo);
                    //}
    });
    

});

router.get('/user/:userid/:password', jsonParser, function (req, res) {//get: get all the urls belongs to this user if it exists
    userService.getUrlByUser(req.params.userid, req.params.password,function (urls) {//this is the call back function due to the ansyncronized IO of nodejs
                    //userInfo will let the controller know if the user is alreay exist or a new one is created
                    //if(userInfo == '0'){//already exists
                    //console.log("test + " + userid);
                        console.log("APIROUTER urls:" + urls.length);
                        res.json(urls);
                    //}
    });
    

});
router.get('/cmp/:CandidateEmail', jsonParser, function (req, res) {
    cmpService.getCanFromMatch(req.params.CandidateEmail,function (Candidates) {
        console.log("APIROUTER:" + Candidates.length);
        
                        res.json(Candidates);
                    
    });
    

});
router.post('/cmp', jsonParser, function (req, res) {
    cmpService.updateMatche(req.body.matcheID,req.body.updatingStage,function (returnCallBack) {
        console.log("Update result:" + returnCallBack);
                        res.json(returnCallBack);
                    
    });
    

});
/*router.get("/user/:userid",function (req, res) {//get: get the long
    var shortUrl = req.params.shortUrl;//from express  
    urlService.getLongUrl(shortUrl, function (url) {
        if (url) {
            res.json(url);//exists, return in json
        }
        else{//no such long url converted or stored in DB
            res.status(404).send("No such long url in DB!");
        }
    });
   
});*/

module.exports = router; //this part is to let the outsider, who used this serviceJS file, use the function in js