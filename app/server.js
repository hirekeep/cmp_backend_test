var express = require('express');
var app = express();
var apiRouter = require('./routes/api');//post short or get long

var indexRouter = require('./routes/indexRouter');//the home page

//MongoDB---
var mongoose = require('mongoose');
mongoose.connect('mongodb://user:user@ds013966.mlab.com:13966/tinyurl');
//---
var useragent = require('express-useragent');//get the clicking info

var sf = require('node-salesforce');
var conn = new sf.Connection({
      // you can change loginUrl to connect to sandbox or prerelease env. 
    loginUrl : 'https://test.salesforce.com' 
});
conn.login('dan@hirekeep.com', '03141994@dc0NHv3cmTD0rtvM2fVUS8BYoGp', function(err, userInfo) {
      if (err) { return console.error(err); }
      // Now you can get the access token and instance URL information. 
      // Save them to establish connection next time. 
      console.log(conn.accessToken);
      console.log(conn.instanceUrl);
      // logged in user property 
      console.log("User ID: " + userInfo.id);
      console.log("Org ID: " + userInfo.organizationId);
      // ... 
});
app.use('/node_modules', express.static(__dirname + "/node_modules"));
app.use('/public', express.static(__dirname + "/public"));//just send file, not excuted so it is static
//

//the reason why we put all websites' view components into the public folder is to avoid confliction with short urls' names
app.use(useragent.express());//the request will be refined by the express useragent
app.use('/api/v1',apiRouter);//to see the long and short url pair by using /api/v1/a_shorturlname
app.use('/',indexRouter);

//console.log("got in server.js");
app.listen(8800);
